package com.example.trackme.modules.record

import com.example.trackme.modules.base.BaseContract
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng

interface IRecordContract {
    interface View : BaseContract.BaseView, OnMapReadyCallback, GoogleMap.InfoWindowAdapter,
        GoogleMap.OnInfoWindowClickListener, GoogleMap.OnPolylineClickListener,
        GoogleMap.OnPolygonClickListener {
        fun pause()
        fun resume()
        fun showLocation(latitude: Double, longitude: Double)
        fun routeMap(arrLatLng: ArrayList<LatLng>)
        fun updateSession(distance: String, speed: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun pause()
        fun resume()
        fun stop()
        fun checkLocationPermissions(): Boolean
        fun getCurrentLocation()
        fun getLastLocation()
        fun removeListenerLocation()
    }

    interface Router : BaseContract.Router {
        fun navigateToHistory()
    }
}