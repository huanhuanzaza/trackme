package com.example.trackme.modules.main.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.trackme.R
import com.example.trackme.entities.Session
import com.example.trackme.modules.history.view.HistoryFragment

class MainActivity : AppCompatActivity() {
    companion object {
        var session: Session? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)
        changeFragment(HistoryFragment())
    }

    fun changeFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()

        ft.replace(R.id.fragment, fragment)
        ft.addToBackStack(fragment::class.java.simpleName)
        ft.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

}