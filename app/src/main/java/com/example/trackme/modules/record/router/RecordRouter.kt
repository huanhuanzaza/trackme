package com.example.trackme.modules.record.router

import android.content.Context
import com.example.trackme.modules.history.view.HistoryFragment
import com.example.trackme.modules.main.view.MainActivity
import com.example.trackme.modules.record.IRecordContract

class RecordRouter(private val context: Context?) : IRecordContract.Router {
    override fun navigateToHistory() {
        (context as MainActivity).changeFragment(HistoryFragment())
    }
}