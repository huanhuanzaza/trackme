package com.example.trackme.modules.dialog

import android.os.Bundle
import android.view.View
import com.example.trackme.R
import com.example.trackme.modules.base.view.BaseDialogFragment

class LoadingFragment : BaseDialogFragment() {
    override fun getViewResource(): View? {
        return activity?.layoutInflater?.inflate(R.layout.fragment_loading, null)
    }

    override fun initialize(savedInstanceState: Bundle?) {

    }
}