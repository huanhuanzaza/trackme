package com.example.trackme.modules.record.view

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.trackme.R
import com.example.trackme.modules.base.reponse.BaseResponse
import com.example.trackme.modules.base.view.BaseFragment
import com.example.trackme.modules.record.IRecordContract
import com.example.trackme.modules.record.presenter.RecordPresenter
import com.example.trackme.utils.map.LocationFinder
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.fragment_record.view.*


class RecordFragment : BaseFragment(), IRecordContract.View, View.OnClickListener {
    private var presenter: RecordPresenter? = null
    private var myMap: GoogleMap? = null
    private var mapView: View? = null

    override fun initializeView(savedInstanceState: Bundle?) {
        initView()
        presenter = RecordPresenter(context, this)
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_record
    }

    override fun onResume() {
        super.onResume()
        presenter?.getLastLocation()
    }

    override fun onPause() {
        super.onPause()
        presenter?.removeListenerLocation()
    }

    private fun initView() {
        v?.btn_pause?.setOnClickListener(this)
        v?.btn_resume?.setOnClickListener(this)
        v?.btn_stop?.setOnClickListener(this)
        val mapFragment = fragmentManager?.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.view
    }

    @SuppressLint("MissingPermission")
    private fun setUpMapView() {
        //Choose Type of map - Normal
        myMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        //Hide Icon zoom in/ out
        myMap?.uiSettings?.isZoomControlsEnabled = false
        //Hide Compass
        myMap?.uiSettings?.isCompassEnabled = false
        //Hide map toolbar (Navigation and GPS pointer)
        myMap?.uiSettings?.isMapToolbarEnabled = false
        myMap?.isMyLocationEnabled = true
    }

    override fun onMapReady(p0: GoogleMap?) {
        myMap = p0
        myMap?.setOnMapLoadedCallback(object : GoogleMap.OnMapLoadedCallback {
            override fun onMapLoaded() {
                if (presenter?.checkLocationPermissions() == true) {
                    setUpMapView()
                }
                presenter?.getCurrentLocation()
            }
        })
        p0?.setOnPolylineClickListener(this)
        p0?.setOnPolygonClickListener(this)
    }

    override fun pause() {
        v?.btn_pause?.visibility = View.GONE
        v?.layout_btn_resume_stop?.visibility = View.VISIBLE
    }

    override fun resume() {
        v?.btn_pause?.visibility = View.VISIBLE
        v?.layout_btn_resume_stop?.visibility = View.GONE
    }

    override fun showLocation(latitude: Double, longitude: Double) {
        if (myMap != null) {
            val latLng = LatLng(latitude, longitude)
            myMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))

            moveCamera(latLng)

            //Add marker for Map
            val option = MarkerOptions()
            option.position(latLng)
            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_location))
            myMap?.setInfoWindowAdapter(this)
            myMap?.setOnInfoWindowClickListener(this)
            val currentMarker = myMap?.addMarker(option)
            currentMarker?.showInfoWindow()
        } else {
            TODO("Dialog show current location fail")
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_pause -> {
                presenter?.pause()
            }

            R.id.btn_resume -> {
                presenter?.resume()
            }

            R.id.btn_stop -> {
                presenter?.stop()
                fragmentManager?.popBackStack()
            }
        }
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return null
    }

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun onInfoWindowClick(p0: Marker?) {
    }

    override fun onPolylineClick(p0: Polyline?) {
    }

    override fun onPolygonClick(p0: Polygon?) {
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val fragment = fragmentManager?.findFragmentById(R.id.map)
        if (fragment != null) {
            activity?.supportFragmentManager?.beginTransaction()
                ?.remove(fragment)
                ?.commit()
        }
    }

    override fun routeMap(arrLatLng: ArrayList<LatLng>) {
        // Add polylines to the map.
        // Polylines are useful to show a route or some other connection between points.
        myMap?.addPolyline(
            PolylineOptions()
                .clickable(true)
                .color(Color.BLUE)
                .addAll(arrLatLng)
        )

        // Position the map's camera near Alice Springs in the center of Australia,
        // and set the zoom factor so most of Australia shows on the screen.
        moveCamera(arrLatLng.last())
    }

    override fun updateSession(distance: String, speed: String) {
        v?.txt_distance_value?.text = distance
        v?.txt_speed_value?.text = speed
    }

    override fun showLoading() {
        TODO("Not yet implemented")
    }

    override fun dismissLoading() {
        TODO("Not yet implemented")
    }

    override fun <T> showErrorMessage(response: BaseResponse<T>?) {
        TODO("Not yet implemented")
    }

    override fun showErrorMessage(messageResId: Int) {
        TODO("Not yet implemented")
    }

    private fun moveCamera(latLng: LatLng?) {
        // Automatically zoom for showing user's location
        val cameraPosition = CameraPosition.Builder()
            .target(latLng)
            .zoom(18f)
            .tilt(40f)
            .build()
        myMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LocationFinder.REQUEST_CODE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter?.getLastLocation()
                }
            }
        }
    }
}