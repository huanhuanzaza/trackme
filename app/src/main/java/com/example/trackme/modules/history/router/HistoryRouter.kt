package com.example.trackme.modules.history.router

import android.content.Context
import com.example.trackme.modules.history.IHistoryContract
import com.example.trackme.modules.main.view.MainActivity
import com.example.trackme.modules.record.view.RecordFragment

class HistoryRouter(private val context: Context?) : IHistoryContract.Router {

    override fun navigateToRecord() {
        (context as MainActivity).changeFragment(RecordFragment())
    }
}