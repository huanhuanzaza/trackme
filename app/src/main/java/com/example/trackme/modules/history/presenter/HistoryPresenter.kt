package com.example.trackme.modules.history.presenter

import android.content.Context
import com.example.trackme.entities.Session
import com.example.trackme.modules.history.IHistoryContract
import com.example.trackme.modules.history.router.HistoryRouter
import com.example.trackme.modules.main.view.MainActivity

class HistoryPresenter(private val context: Context?) : IHistoryContract.Presenter {
    private val router = HistoryRouter(context)
    override fun record() {
        val session: Session? = null
        session?.status = Session.STATUS_RECORDING
        session?.createdAt = System.currentTimeMillis()

        MainActivity.session = session
        router.navigateToRecord()
    }
}