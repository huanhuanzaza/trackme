package com.example.trackme.modules.base.reponse

import com.google.gson.annotations.SerializedName

open class BaseErrorResponse {

    @SerializedName("errorCode")
    var errorCode: Int? = -1

    @SerializedName("errorMessage")
    var errorMessage: String? = null

}