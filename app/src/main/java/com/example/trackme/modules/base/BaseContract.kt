package com.example.trackme.modules.base

import androidx.annotation.StringRes
import com.example.trackme.modules.base.reponse.BaseResponse

interface BaseContract {
    interface BaseView {
        fun showLoading()
        fun dismissLoading()
        fun <T> showErrorMessage(response: BaseResponse<T>?)
        fun showErrorMessage(@StringRes messageResId: Int)
    }
    interface Presenter
    interface Router
}