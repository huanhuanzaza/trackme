package com.example.trackme.modules.history

import com.example.trackme.modules.base.BaseContract

class IHistoryContract {
    interface View : BaseContract.BaseView
    interface Presenter : BaseContract.Presenter {
        fun record()
    }

    interface Router : BaseContract.Router {
        fun navigateToRecord()
    }
}