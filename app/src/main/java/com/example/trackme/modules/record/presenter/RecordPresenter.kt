package com.example.trackme.modules.record.presenter

import android.content.Context
import android.location.Location
import android.util.Log
import com.example.trackme.modules.main.view.MainActivity
import com.example.trackme.modules.record.IRecordContract
import com.example.trackme.modules.record.router.RecordRouter
import com.example.trackme.utils.map.LocationFinder
import com.google.android.gms.maps.model.LatLng
import java.text.DecimalFormat
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


class RecordPresenter(private var context: Context?, private val view: IRecordContract.View) :
    IRecordContract.Presenter {
    private var currentLocation: Location? = null
    private val router = RecordRouter(context)
    private val locationFinder = LocationFinder(context)
    private var arrLatLng = ArrayList<LatLng>()
    private val listener = object : LocationFinder.ILocationListener {
        override fun onLocationChanged(location: Location?) {
            if (location != null) {
                arrLatLng.add(LatLng(location.latitude, location.longitude))
                if (currentLocation != null) {
                    val distance = calculationByDistance(currentLocation!!, location)
                    if (distance >= LocationFinder.MIN_DISTANCE_CHANGE_FOR_UPDATES) {
                        currentLocation = location
                        view.routeMap(arrLatLng)
                        updateSessionSpeed(distance)
                    }
                } else {
                    MainActivity.session?.startLocation = location
                }
            }
        }

    }

    override fun pause() {
        view.pause()
    }

    override fun resume() {
        view.resume()
    }

    override fun stop() {
        router.navigateToHistory()
    }

    override fun getCurrentLocation() {
        val location = locationFinder.getLocation()
        if (location?.latitude != null) {
            arrLatLng.add(LatLng(location.latitude, location.longitude))
            currentLocation = location
            view.showLocation(location.latitude, location.longitude)
        }
    }

    override fun getLastLocation() {
        locationFinder.setListener(listener)
        locationFinder.getLocation()
    }

    override fun removeListenerLocation() {
        locationFinder.removeUpdates()
    }

    override fun checkLocationPermissions(): Boolean {
        return locationFinder.checkLocationPermission()
    }

    private fun updateSessionSpeed(distance: Double) {
        val distanceInKm = MainActivity.session?.distance?.plus(distance)
        val time = (System.currentTimeMillis() - MainActivity.session?.createdAt!!) / 3600000.0
        if (distanceInKm != null) {
            val speed = distanceInKm / time
            MainActivity.session?.distance = distanceInKm
            MainActivity.session?.speed = speed
            view.updateSession(String.format("%.2f", distanceInKm), String.format("%.2f", speed))
        }
    }

    private fun calculationByDistance(startLocation: Location, endLocation: Location): Double {

        val startPoint = LatLng(startLocation.latitude, startLocation.longitude)
        val endPoint = LatLng(endLocation.latitude, endLocation.longitude)

        val radius = 6371 // radius of earth in Km
        val lat1 = startPoint.latitude
        val lat2 = endPoint.latitude
        val lon1 = startPoint.longitude
        val lon2 = endPoint.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = (sin(dLat / 2) * sin(dLat / 2)
                + (cos(Math.toRadians(lat1))
                * cos(Math.toRadians(lat2)) * sin(dLon / 2)
                * sin(dLon / 2)))
        val c = 2 * asin(sqrt(a))
        val valueResult = radius * c
        val km = valueResult / 1
        val newFormat = DecimalFormat("####")
        val kmInDec: Int = Integer.valueOf(newFormat.format(km))
        val meter = valueResult % 1000
        val meterInDec: Int = Integer.valueOf(newFormat.format(meter))
        Log.i(
            "Radius Value", "" + valueResult + "   KM  " + kmInDec
                    + " Meter   " + meterInDec
        )
        return radius * c
    }

}