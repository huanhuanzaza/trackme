package com.example.trackme.modules.base.reponse

import com.google.gson.annotations.SerializedName

class BaseResponse<T> :
    BaseErrorResponse {

    @SerializedName("messageResId")
    var messageResId: Int? = null
    @SerializedName("data")
    var data: T? = null

    constructor(errorCode: Int?, errorMessage: String?, data: T?) {
        this.errorCode = errorCode
        this.errorMessage = errorMessage
        this.data = data
    }

    constructor(errorCode: Int?, errorMessage: String?, messageResId: Int?) {
        this.errorCode = errorCode
        this.errorMessage = errorMessage
        this.messageResId = messageResId
    }
}