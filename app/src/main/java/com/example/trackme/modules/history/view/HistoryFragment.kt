package com.example.trackme.modules.history.view

import android.os.Bundle
import android.view.View
import com.example.trackme.R
import com.example.trackme.modules.base.reponse.BaseResponse
import com.example.trackme.modules.base.view.BaseFragment
import com.example.trackme.modules.history.IHistoryContract
import com.example.trackme.modules.history.presenter.HistoryPresenter
import kotlinx.android.synthetic.main.fragment_history.view.*

class HistoryFragment : BaseFragment(), IHistoryContract.View, View.OnClickListener {

    private var presenter: HistoryPresenter? = null

    override fun initializeView(savedInstanceState: Bundle?) {
        initView()
        presenter = HistoryPresenter(context)
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_history
    }

    private fun initView() {
        v?.btn_record?.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_record -> {
                presenter?.record()
            }
        }
    }

    override fun showLoading() {
        TODO("Not yet implemented")
    }

    override fun dismissLoading() {
        TODO("Not yet implemented")
    }

    override fun <T> showErrorMessage(response: BaseResponse<T>?) {
        TODO("Not yet implemented")
    }

    override fun showErrorMessage(messageResId: Int) {
        TODO("Not yet implemented")
    }

}