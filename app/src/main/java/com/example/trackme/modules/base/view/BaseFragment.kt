package com.example.trackme.modules.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    var v: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = LayoutInflater.from(activity).inflate(getLayoutResource(), container, false)

            initializeView(savedInstanceState)
        }

        return v
    }

    abstract fun initializeView(savedInstanceState: Bundle?)
    abstract fun getLayoutResource(): Int
}