package com.example.trackme.entities

import android.location.Location
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

@Entity(tableName = "session")
class Session {
    companion object {
        const val STATUS_RECORDING = 0
        const val STATUS_PAUSE = 1
        const val STATUS_PENDING = 2
        const val STATUS_COMPLETED = 3
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    var id: Int? = null

    @ColumnInfo(name = "status")
    @SerializedName("status")
    var status: Int? = null

    @ColumnInfo(name = "startLocation")
    @SerializedName("startLocation")
    var startLocation: Location? = null

    @ColumnInfo(name = "speed")
    @SerializedName("speed")
    var speed: Double? = null

    @ColumnInfo(name = "distance")
    @SerializedName("distance")
    var distance: Double = 0.0

    @ColumnInfo(name = "durations")
    @SerializedName("durations")
    var durations: Int? = null

    @ColumnInfo(name = "latLngs")
    @SerializedName("latLngs")
    var latLngs: ArrayList<LatLng>? = null

    @ColumnInfo(name = "createdAt")
    @SerializedName("createdAt")
    var createdAt: Long? = null

}