package com.example.trackme.utils.map

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.example.trackme.R
import com.example.trackme.modules.dialog.MessageDialogFragment

class LocationFinder(private val context: Context?) {
    companion object {
        const val REQUEST_CODE_LOCATION = 3000
        const val MIN_DISTANCE_CHANGE_FOR_UPDATES = 0.1F //10 meters
        const val MIN_TIME_BW_UPDATES = 200 * 10 * 1L //2 seconds
    }

    private var locationManager: LocationManager? = null
    private var listener: ILocationListener? = null

    fun setListener(listener: ILocationListener?) {
        this.listener = listener
    }

    fun checkLocationPermission(): Boolean {
        context?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    val permissionList = arrayOf(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    )
                    (context as? Activity)?.requestPermissions(
                        permissionList,
                        REQUEST_CODE_LOCATION
                    )
                    return false
                }
            }
        }
        return true
    }

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location?) {
            if (location != null) {
                listener?.onLocationChanged(location)
            }
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

        override fun onProviderEnabled(provider: String?) {

        }

        override fun onProviderDisabled(provider: String?) {

        }

    }

    fun removeUpdates() {
        locationManager?.removeUpdates(locationListener)
    }

    fun getLocation(): Location? {
        var location: Location? = null
        var isUpdatedLocation: Boolean? = null
        if (checkLocationPermission()) {
            try {
                locationManager =
                    (context as? Activity)?.getSystemService(Context.LOCATION_SERVICE) as? LocationManager

                //getting GPS status
                val isGPSEnabled =
                    locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) ?: false

                //getting network status
                val isNetworkEnabled =
                    locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ?: false

                if (!isNetworkEnabled && !isGPSEnabled) {
                    //no network and gps
                    showSettingsAlert()
                } else {
                    if (isGPSEnabled) {
                        locationManager?.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener
                        )
                        location =
                            locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                        Log.d(
                            "huanhuan",
                            "-- GPR LOCATION : " + location?.latitude + "  -  " + location?.longitude
                        )
                        Log.d(
                            "huanhuan",
                            "-- GPR TIME : " + location?.time
                        )
                        isUpdatedLocation = checkUpdateTime(location?.time)
                    }

                    if ((location == null || isUpdatedLocation == null || !isUpdatedLocation) && isNetworkEnabled) {
                        locationManager?.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener
                        )
                        location =
                            locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                        Log.d(
                            "huanhuan",
                            "-- NETWORK LOCATION : " + location?.latitude + "  -  " + location?.longitude
                        )
                    }
                }
            } catch (e: Exception) {

            }
        }
        return location
    }

    private fun showSettingsAlert() {
        val dialog = MessageDialogFragment.newInstance(
            "",
            context?.resources?.getString(R.string.gps_is_not_enabled) ?: ""
        )
        dialog.setMultiChoice(
            context?.resources?.getString(R.string.btn_no) ?: "",
            context?.resources?.getString(R.string.btn_yes) ?: ""
        )
        dialog.listener = object : MessageDialogFragment.MessageDialogListener {
            override fun onNegativeClickListener(dialogFragment: DialogFragment) {

            }

            override fun onPositiveClickListener(dialogFragment: DialogFragment) {
                dialog.dismiss()
                context?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

            override fun onNeutralClickListener(dialogFragment: DialogFragment) {

            }
        }
        if ((context as? FragmentActivity)?.supportFragmentManager != null) {
            dialog.show((context as? FragmentActivity)?.supportFragmentManager!!, "")
        }
    }

    private fun checkUpdateTime(locationTime: Long?): Boolean {
        if (locationTime == null) {
            return false
        }
        val currentTime = System.currentTimeMillis()
        val rangeTime = currentTime - locationTime
        return rangeTime < MIN_TIME_BW_UPDATES
    }

    interface ILocationListener {
        fun onLocationChanged(location: Location?)
    }
}