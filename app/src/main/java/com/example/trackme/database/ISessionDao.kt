package com.example.trackme.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.example.trackme.entities.Session

@Dao
interface ISessionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSession(session: Session)
}