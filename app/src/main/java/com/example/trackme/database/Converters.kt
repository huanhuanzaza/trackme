package com.example.trackme.database

import android.location.Location
import androidx.room.TypeConverter
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun fromArrayLatLngToString(latLngs: ArrayList<LatLng>?): String {
        return Gson().toJson(latLngs)
    }

    @TypeConverter
    fun fromStringToArrayLatLng(data: String): ArrayList<LatLng>? {
        val type = object : TypeToken<ArrayList<LatLng>>() {}.type
        return Gson().fromJson(data, type)
    }

    @TypeConverter
    fun fromLocationToString(location: Location?): String {
        return Gson().toJson(location)
    }

    @TypeConverter
    fun fromStringToLocation(data: String): Location {
        val type = object : TypeToken<Location>() {}.type
        return Gson().fromJson(data, type)
    }

}